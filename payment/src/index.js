import React from "react";
import ReactDOM from "react-dom";
import App from "./components/App";

import { Provider } from "react-redux";

import { createStore, applyMiddleware } from "redux";
import thunkMiddleware from "redux-thunk";
import PaymentReducer from "./reducer/PaymentReducer";

const store = createStore(PaymentReducer, applyMiddleware(thunkMiddleware)); // needs reducers
console.log("store is ", store.getState());

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);
