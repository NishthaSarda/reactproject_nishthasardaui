import AddPaymentDetails from "./AddPaymentDetails";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import PaymnetData from "./PaymentData";
import Navbar from "./Navbar";
import "./App.css";
import ApplicationStatus from "./ApplicationStatus";
import FindByType from "./FindByType";
const App = () => {
  return (
    <Router>
      <div className="app">
        <Navbar />
        <Switch>
          <Route path="/add">
            <AddPaymentDetails />
          </Route>
          <Route exact path="/">
            <ApplicationStatus />
          </Route>
          <Route path="/details">
            <PaymnetData />
          </Route>
          <Route path="/findbytype">
            <FindByType />
          </Route>
        </Switch>
      </div>
    </Router>
  );
};

export default App;
