import React, { useState, useEffect } from "react";
import axios from "axios";
import ReactDom from "react-dom";

const ApplicationStatus = () => {
  const [paymentAppStatus, setPaymentAppStatus] = useState([]);

  useEffect(() => {
    axios.get("http://localhost:8080/api/payment/status").then((resp) => {
      setPaymentAppStatus(resp.data);
    });
  });
  return <div>{paymentAppStatus}</div>;
};
export default ApplicationStatus;
