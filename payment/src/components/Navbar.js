import React from "react";
import { NavLink } from "react-router-dom";

const Navbar = () => (
  <ul className="nav">
    <li className="nav-item">
      <NavLink
        exact
        className="nav-link text-secondary"
        to="/"
        activeClassName="app"
      >
        Home
      </NavLink>
    </li>
    <li className="nav-item">
      <NavLink
        exact
        className="nav-link text-secondary"
        to="/details"
        activeClassName="app"
      >
        PaymentDetails
      </NavLink>
    </li>
    <li className="nav-item">
      <NavLink
        exact
        className="nav-link text-secondary"
        to="/findbytype"
        activeClassName="app"
      >
        FindByType
      </NavLink>
    </li>
    <li className="nav-item">
      <NavLink
        exact
        className="nav-link text-secondary"
        to="/add"
        activeClassName="app"
      >
        Add PaymentDetails
      </NavLink>
    </li>
  </ul>
);

export default Navbar;
