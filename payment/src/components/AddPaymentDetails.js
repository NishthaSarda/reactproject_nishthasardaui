import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { addPAYMENT } from "../actions/paymentAction";

import { useHistory } from "react-router-dom";

const AddPaymentDetails = () => {
  const dispatch = useDispatch();

  const history = useHistory();
  const [id, setId] = useState("");
  const [paymentDate, setPaymentDate] = useState("");
  const [amount, setAmount] = useState("");
  const [custId, setCustId] = useState("");
  const [type, setType] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(id, paymentDate, amount, custId, type);
    let paymentReqbody = {
      id: id,
      paymentDate: paymentDate,
      amount: amount,
      type: type,
      custId: custId,
    };
    dispatch(addPAYMENT(paymentReqbody))
      .then(() => {
        console.log("add Paymnet is successful");
      })
      .catch(() => {})
      .finally(() => {
        console.log("addPayment thunk function is completed");
        history.push("/");
      });
  };
  return (
    <div className="container" style={{ marginTop: 10, marginBottom: 150 }}>
      <h3>Add New Payment Details</h3>
      <form onSubmit={handleSubmit} autoComplete="off">
        <div className="form-row">
          <div className="form-group col-md-5">
            <label htmlFor="id">ID:</label>
            <input
              id="id"
              type="number"
              className="form-control"
              value={id}
              onChange={(e) => setId(e.target.value)}
            />
          </div>
          <div className="form-group col-md-5">
            <label htmlFor="paymnetDate">Payment Date:</label>
            <input
              id="paymentDate"
              type="date"
              className="form-control"
              value={paymentDate}
              onChange={(e) => setPaymentDate(e.target.value)}
            />
          </div>
          <div className="form-group col-md-5">
            <label htmlFor="type">Type:</label>
            <input
              id="type"
              type="text"
              className="form-control"
              value={type}
              onChange={(e) => setType(e.target.value)}
            />
          </div>
        </div>
        <div className="form-row">
          <div className="form-group col-md-5">
            <label htmlFor="amount">Amount:</label>
            <input
              id="amount"
              type="number"
              className="form-control"
              value={amount}
              onChange={(e) => setAmount(e.target.value)}
            />
          </div>
          <div className="form-group col-md-5">
            <label htmlFor="custId">Customer Id:</label>
            <input
              id="custId"
              type="number"
              className="form-control"
              value={custId}
              onChange={(e) => setCustId(e.target.value)}
            />
          </div>
        </div>
        <div className="form-row">
          <div className="form-group col-md-5">
            <input
              type="submit"
              value="Add Details"
              className="btn btn-outline-secondary"
            />
          </div>
        </div>
      </form>
    </div>
  );
};
export default AddPaymentDetails;
