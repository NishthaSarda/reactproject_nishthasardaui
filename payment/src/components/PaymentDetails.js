import React from "react";

const PaymentDetails = ({ paymentDetail }) => {
  return (
    <div className="col-md-4">
      <div className="card mb-4 box-shadow" style={{ width: "18rem" }}>
        <div className="card-body"></div>
        <h1>ID: {paymentDetail.id}</h1>
        <h5>Payment Date: {paymentDetail.paymentDate}</h5>
        <h5>Payment Type: {paymentDetail.type}</h5>
        <h5>Payment Amount: {paymentDetail.amount}</h5>
        <h5>CustId: {paymentDetail.custId}</h5>
      </div>
    </div>
  );
};

export default PaymentDetails;
