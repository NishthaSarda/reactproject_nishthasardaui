import React, { useState, useEffect } from "react";
import axios from "axios";
import PaymentDetails from "./PaymentDetails";

const FindByType = () => {
  const [paymentData, setPaymentData] = useState([]);
  const [type, setType] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    if (type !== "") {
      axios
        .get("http://localhost:8080/api/payment/findByType/" + type)
        .then((resp) => {
          setPaymentData(resp.data);
        });
    }
  };
  return (
    <div>
      <div className="form-group col-md-5">
        <label htmlFor="type"> Select Type:</label>
        <select
          id="type"
          name="type"
          className="form-control"
          onChange={(e) => setType(e.target.value)}
        >
          <option value="">Select</option>
          <option value="Debit">Debit</option>
          <option value="Credit">Credit</option>
        </select>
        <button onClick={handleSubmit}>Find By Type</button>
      </div>

      <div className="container">
        <div className="row">
          {paymentData.map((data) => (
            <PaymentDetails key={data.id} paymentDetail={data}></PaymentDetails>
          ))}
        </div>
      </div>
    </div>
  );
};
export default FindByType;
