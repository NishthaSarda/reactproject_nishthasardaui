import React, { useState, useEffect } from "react";
import PaymentDetails from "./PaymentDetails";
import { useDispatch } from "react-redux";
import { fetchPaymentData } from "../actions/paymentAction";
import { useSelector } from "react-redux";

const PaymnetData = () => {
  const dispatch = useDispatch();
  dispatch(fetchPaymentData());
  const paymentData = useSelector((state) => state.entities);

  return (
    <div className="container">
      <div className="row">
        {paymentData.map((data) => (
          <PaymentDetails key={data.id} paymentDetail={data}></PaymentDetails>
        ))}
      </div>
    </div>
  );
};

export default PaymnetData;
