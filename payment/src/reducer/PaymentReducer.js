const initialState = { entities: [] };
const PaymentReducer = (state = initialState, action) => {
  console.log(`received ${action.type} dispatch in paymentReducer`);
  switch (action.type) {
    case "FETCH_PAYMENT_BEGIN":
    case "ADD_PAYMENT_BEGIN":
      return { ...state, loading: true, error: null };
    case "FETCH_PAYMENT_SUCCESS":
      return { ...state, entities: action.payload, loading: false };
    case "FETCH_PAYMENT_FAILURE":
      return { ...state, entities: [], loading: false, error: action.payload };
    case "ADD_PAYMENT_SUCCESS":
      return { ...state, loading: false };
    case "ADD_PAYMENT_FAILURE":
      return { ...state, loading: false, error: action.payload };
    default:
      return state;
  }
};
export default PaymentReducer;
