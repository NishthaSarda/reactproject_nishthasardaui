import axios from "axios";

// for our Action Creators
export const fetchPaymentDataBegin = () => {
  return {
    type: "FETCH_PAYMENT_BEGIN",
  };
};

export const fetchPaymentDataSuccess = (PaymentData) => {
  return {
    type: "FETCH_PAYMENT_SUCCESS",
    payload: PaymentData,
  };
};

export const fetchPaymentDataFailure = (err) => {
  return {
    type: "FETCH_PAYMENT_FAILURE",
    payload: {
      message: "Failed to fetch PaymentData.. please try again later",
    },
  };
};

// to be call by the components
export const fetchPaymentData = () => {
  // returns the thunk function
  return (dispatch, getState) => {
    dispatch(fetchPaymentDataBegin());
    console.log("state after fetchPaymentDataBegin", getState());
    axios.get("http://localhost:8080/api/payment/all").then(
      (res) => {
        setTimeout(() => {
          dispatch(fetchPaymentDataSuccess(res.data));
          console.log("state after fetchPaymentDataSuccess", getState());
        }, 3000);
      },
      (err) => {
        dispatch(fetchPaymentDataFailure(err));
        console.log("state after fetchPaymentDataFailure", getState());
      }
    );
  };
};

export const addPaymentDataBegin = () => {
  return {
    type: "ADD_PAYMENT_BEGIN",
  };
};

export const addPaymentDatasuccess = () => {
  return {
    type: "ADD_PAYMENT_SUCCESS",
  };
};

export const addPaymentFailure = (err) => {
  return {
    type: "ADD_PAYMENT_FAILURE",
    payload: { message: "Failed to add new payment.. please try again later" },
  };
};

function delay(t, v) {
  return new Promise(function (resolve) {
    setTimeout(resolve.bind(null, v), t);
  });
}

export const addPAYMENT = (payment) => {
  // returns our async thunk function
  return (dispatch, getState) => {
    return axios.post("http://localhost:8080/api/payment/save", payment).then(
      () => {
        console.log("PAYMENT created!");
        // this is where we can dispatch ADD_PAYMENT_SUCCESS
        dispatch(addPaymentDatasuccess());
      },
      (err) => {
        dispatch(addPaymentFailure(err));
        console.log("state after addPAYMENTFailure", getState());
      }
    );
  };
};
